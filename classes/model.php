<?php
namespace Rebel;

Class Model extends \Model {

	function save() {
		$this->before_save();
		parent::save();
		$this->after_save();
		return $this;
	}

	function before_save() {
		echo 'before';
	}

	function after_save() {
		echo 'after';
	}

}